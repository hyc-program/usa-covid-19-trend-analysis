**- 研究目的：**

探討美國在開始施打新冠病毒疫苗後，確診人數有無變化。

**- 研究方法：**

本研究採用[CRISP](https://en.wikipedia.org/wiki/Cross-industry_standard_process_for_data_mining)方法探討疫苗施打對於控制疫情的成果
CRISP 研究流程主要分成商業解析、資料解析、資料準備、模型預測、評估以及部屬。
本研究主要針對商業解析，資料解析，資料準備，模型預測以及評估為主，其中模型預測的部分，則以圖表分析作為替代。
![CRISP流程圖](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/CRISP-DM_Process_Diagram.png/598px-CRISP-DM_Process_Diagram.png)



**- 研究分析：**

1. 商業解析

本研究的目的是希望探討新冠疫苗的施打是否對疫情有正面的助益。

2. 資料解析

為了能夠探討疫苗施打是否對於疫情能有正面的助益，必須有以下幾點資料
    1. 疫苗施打日期
    2. 確診人數的歷史資料

3. 資料準備

在施打疫苗日期的資料上，資料來源取自 [BBC新聞網](https://www.bbc.com/news/world-us-canada-55305720)
從[The COVID Tracking Project](https://covidtracking.com/data/download)網站中，如下表可以獲得以下相關資料
因此在資料的準備上，是能夠協助做相關分析的


| 資料名稱 | 資料型別 |
| ------ | ------ |
| date | 時間 |
| positive | 整數 |



4. 圖表分析

進行圖表分析前，必須進行資料處理(細節麻煩參照[Gitlab專案](https://gitlab.com/hyc-program/usa-covid-19-trend-analysis/-/blob/master/covid19_data_test_v2.py) USA Covid-19 Trend Analysis的程式碼)
在資料處理上，由於單日的確診資料太過細緻，因此將資料以月分為單位進行切割，並透過pandas library group by的套件進行資料的分群。
資料處理後，再透過在[Colab平台上](https://colab.research.google.com/drive/1N8NiADbkEqupIHchjbbScKtoRwYMH8Pk#scrollTo=wvxQfXEEIm-J)引入pyplot的套件來製作確診人數(y軸)以及時間月份(x軸)的相關關係圖。
資料的呈現結果如下

![chart](./covid19_analysis_result.png)

5. 結論

根據BBC新聞網的資訊，美國在2020年12月14日開始施打新冠病毒疫苗，
而從上面圖表中可以得知，大約從2021年1月底接近2月開始，確診人數有明顯的下滑趨勢，
因此，可以判斷美國的新冠病毒疫苗有正面的效果。

6. 改進

因為資料的起始點並非該月的第一天，末端該月的資料也並非完整，
因此若以月分為單位切割後進行分析，有失資料的準確性，日後仍需針對該點進行調整改進。


**- 資料來源**

Covid-19: First vaccine given in US as roll-out begins.檢自https://www.bbc.com/news/world-us-canada-55305720 (Nov.
14, 2020)

The COVID Tracking Project.檢自https://covidtracking.com/data/download (Mar. 7, 2021)

Cross-industry standard process for data mining.檢自https://en.wikipedia.org/wiki/Cross-industry_standard_process_for_data_mining (Nov. 15, 2020)
